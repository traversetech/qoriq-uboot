/*
 * Copyright 2015 Freescale Semiconductor
 * Copyright 2017 Traverse Technologies
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __LS1043V_H__
#define __LS1043V_H__


#include "ls1043a_common.h"

#if defined(MTDIDS_DEFAULT)
#undef MTDIDS_DEFAULT
#endif

#define MTDIDS_DEFAULT "nand0=7e800000.flash"

#if defined(MTDPARTS_DEFAULT)
#undef MTDPARTS_DEFAULT
#endif
#define MTDPARTS_DEFAULT "mtdparts=7e800000.flash:1m(nand_uboot),384k(nand_uboot_env),128k(fman_fw),256k(ppa),128k(dtb),-(nandubi)"

#if defined(CONFIG_EXTRA_ENV_SETTINGS)
#undef CONFIG_EXTRA_ENV_SETTINGS
#endif

#ifndef CONFIG_SPL_BUILD
#define CONFIG_EXTRA_ENV_SETTINGS               \
        "hwconfig=fsl_ddr:bank_intlv=auto\0"    \
        "load_addr=0xa0000000\0"                \
        "fdt_high=0xffffffffffffffff\0"         \
        "initrd_high=0xffffffffffffffff\0"      \
        "kernel_load=0xa0000000\0"              \
        "console=ttyS0,115200\0"                \
		"fdt_high=0xffffffffffffffff\0"			\
		"initrd_high=0xffffffffffffffff\0"		\
		"kernel_addr_r=0x81000000\0"			\
		"fdt_addr_r=0x90000000\0"				\
        "kernelheader_addr_r=0x80200000\0"		\
		"fdtheader_addr_r=0x80100000\0"			\
		"scripthdraddr=0x80080000\0"			\
		"scriptaddr=0x80000000\0"				\
		"mtdparts=" MTDPARTS_DEFAULT "\0"	\
	"bootsys=1\0" \
	"mtdids=" MTDIDS_DEFAULT "\0"		\
	"bootargs=console=ttyS0,115200 earlycon=uart8250,mmio,0x21c0500\0" \
	BOOTENV			\
	"createnandparts=ubi part nandubi; ubi create kernel1 0x400000; ubi create rootfs1 0x8000000; ubi create kernel2 0x400000; ubi create rootfs2 0x8000000\0"	\
	"loadimage=ubi part nandubi; tftpboot $kernel_load lede-layerscape-64b.itb; ubi write $kernel_load kernel1 $filesize; tftpboot $kernel_load lede-layerscape-64b-root.ubifs; ubi write $kernel_load rootfs1 $filesize\0" \
	"nandbootargs=setenv bootargs console=ttyS0,115200 earlycon=uart8250,mmio,0x21c0500 root=ubi0:rootfs$bootsys rootfstype=ubifs rootwait "MTDPARTS_DEFAULT " ubi.mtd=nandubi\0" \
	"nandboot=run nandbootargs && ubi part nandubi && ubi read $kernel_load kernel$bootsys && bootm $kernel_load\0" \
	"sdboot=fatload mmc 0 $kernel_load uEnv.txt && env import $kernel_load $filesize && boot\0" \
	"usbboot=usb start && fatload usb 0 $kernel_load uEnv.txt && env import $kernel_load $filesize && boot\0"  \
	"distroboot=nand read ${fdt_addr_r} dtb && fdt addr ${fdt_addr_r} && fdt resize 512 && fdt chosen && run distro_bootcmd\0"
#else
#define CONFIG_ENV_EXTRA_SETTINGS "\0"
#endif

#undef CONFIG_BOOTCOMMAND

#define CONFIG_BOOTCOMMAND "run distroboot || run nandboot"

/* For NAND boot, this is set by run nandbootargs above, so CONFIG_BOOTARGS should mirror it as a fallback */
#define CONFIG_BOOTARGS			"console=ttyS0,115200 " \
 					"earlycon=uart8250,mmio,0x21c0500 "    \
					"ubi.mtd=nandubi root=ubi0:rootfs1 rootfstype=ubifs rootwait " \
 					MTDPARTS_DEFAULT


#if defined(CONFIG_NAND_BOOT) || defined(CONFIG_SD_BOOT)
#define CONFIG_SYS_TEXT_BASE		0x82000000
#else
#define CONFIG_SYS_TEXT_BASE		0x60100000
#endif

#define CONFIG_SYS_CLK_FREQ		100000000
#define CONFIG_DDR_CLK_FREQ		100000000

#define CONFIG_LAYERSCAPE_NS_ACCESS
#define CONFIG_MISC_INIT_R

#define CONFIG_DIMM_SLOTS_PER_CTLR	1
/* Physical Memory Map */
#define CONFIG_CHIP_SELECTS_PER_CTRL	4
#define CONFIG_NR_DRAM_BANKS		2

#define CONFIG_SYS_SPD_BUS_NUM		0

#define CONFIG_FSL_DDR_BIST
#ifndef CONFIG_SPL
#define CONFIG_FSL_DDR_INTERACTIVE	/* Interactive debugging */
#endif
#define CONFIG_SYS_DDR_RAW_TIMING
#define CONFIG_ECC_INIT_VIA_DDRCONTROLLER
#define CONFIG_MEM_INIT_VALUE           0xdeadbeef

#ifdef CONFIG_RAMBOOT_PBL
#define CONFIG_SYS_FSL_PBL_PBI board/freescale/ls1043ardb/ls1043ardb_pbi.cfg
#endif

#ifdef CONFIG_NAND_BOOT
#define CONFIG_SYS_FSL_PBL_RCW board/traverse/ls1043v/ls1043v_rcw_nand_1600.cfg
#endif

#ifdef CONFIG_SD_BOOT
#define CONFIG_SYS_FSL_PBL_RCW board/traverse/ls1043v/ls1043v_rcw_nand_1600.cfg
#endif

#ifdef CONFIG_NAND_BOOT
/* Store Fman ucode at offeset 0x160000(11 blocks) - this is different to the LS1043ARDB setting */
#define CONFIG_SYS_QE_FMAN_FW_IN_NAND
#undef CONFIG_SYS_FMAN_FW_ADDR
#define CONFIG_SYS_FMAN_FW_ADDR		(11 * CONFIG_SYS_NAND_BLOCK_SIZE)
#endif 

#define CONFIG_SYS_MAX_FLASH_BANKS	1	/* number of banks */
#define CONFIG_SYS_MAX_FLASH_SECT	1024	/* sectors per device */
#define CONFIG_SYS_FLASH_ERASE_TOUT	60000	/* Flash Erase Timeout (ms) */
#define CONFIG_SYS_FLASH_WRITE_TOUT	500	/* Flash Write Timeout (ms) */

#define CONFIG_SYS_FLASH_EMPTY_INFO
#define CONFIG_SYS_FLASH_BANKS_LIST	{ CONFIG_SYS_FLASH_BASE_PHYS }

#define CONFIG_CFI_FLASH_USE_WEAK_ACCESSORS
#define CONFIG_SYS_WRITE_SWAPPED_DATA

/*
 * NAND Flash Definitions
 */
#ifndef SPL_NO_IFC
#define CONFIG_NAND_FSL_IFC
#endif

#define CONFIG_SYS_NAND_BASE		0x7e800000
#define CONFIG_SYS_NAND_BASE_PHYS	CONFIG_SYS_NAND_BASE

#define CONFIG_SYS_NAND_CSPR_EXT	(0x0)
#define CONFIG_SYS_NAND_CSPR	(CSPR_PHYS_ADDR(CONFIG_SYS_NAND_BASE_PHYS) \
				| CSPR_PORT_SIZE_8	\
				| CSPR_MSEL_NAND	\
				| CSPR_V)
#define CONFIG_SYS_NAND_AMASK	IFC_AMASK(128*128*1024)
#define CONFIG_SYS_NAND_CSOR	(CSOR_NAND_ECC_ENC_EN	/* ECC on encode */ \
				| CSOR_NAND_ECC_DEC_EN	/* ECC on decode */ \
				| CSOR_NAND_ECC_MODE_4	/* 4-bit ECC */ \
				| CSOR_NAND_RAL_3	/* RAL = 3 Bytes */ \
				| CSOR_NAND_PGS_2K	/* Page Size = 2K */ \
				| CSOR_NAND_SPRZ_64	/* Spare size = 64 */ \
				| CSOR_NAND_PB(64))	/* 64 Pages Per Block */

#define CONFIG_SYS_NAND_ONFI_DETECTION

#define CONFIG_SYS_NAND_FTIM0		(FTIM0_NAND_TCCST(0x7) | \
					FTIM0_NAND_TWP(0x18)   | \
					FTIM0_NAND_TWCHT(0x7) | \
					FTIM0_NAND_TWH(0xa))
#define CONFIG_SYS_NAND_FTIM1		(FTIM1_NAND_TADLE(0x32) | \
					FTIM1_NAND_TWBE(0x39)  | \
					FTIM1_NAND_TRR(0xe)   | \
					FTIM1_NAND_TRP(0x18))
#define CONFIG_SYS_NAND_FTIM2		(FTIM2_NAND_TRAD(0xf) | \
					FTIM2_NAND_TREH(0xa) | \
					FTIM2_NAND_TWHRE(0x1e))
#define CONFIG_SYS_NAND_FTIM3		0x0

#define CONFIG_SYS_NAND_BASE_LIST	{ CONFIG_SYS_NAND_BASE }
#define CONFIG_SYS_MAX_NAND_DEVICE	1
#define CONFIG_MTD_NAND_VERIFY_WRITE
#define CONFIG_MTD_PARTITIONS
#define CONFIG_MTD_DEVICE

#define CONFIG_SYS_NAND_BLOCK_SIZE	(128 * 1024)

#if defined(CONFIG_NAND_BOOT)
#define CONFIG_SPL_PAD_TO		0x20000		/* block aligned */
#define CONFIG_SYS_NAND_U_BOOT_OFFS	CONFIG_SPL_PAD_TO
#define CONFIG_SYS_NAND_U_BOOT_SIZE	(1024 << 10)
#endif

/* IFC Timing Params */
/* #ifdef CONFIG_NAND_BOOT */
#define CONFIG_SYS_CSPR0_EXT		CONFIG_SYS_NAND_CSPR_EXT
#define CONFIG_SYS_CSPR0		CONFIG_SYS_NAND_CSPR
#define CONFIG_SYS_AMASK0		CONFIG_SYS_NAND_AMASK
#define CONFIG_SYS_CSOR0		CONFIG_SYS_NAND_CSOR
#define CONFIG_SYS_CS0_FTIM0		CONFIG_SYS_NAND_FTIM0
#define CONFIG_SYS_CS0_FTIM1		CONFIG_SYS_NAND_FTIM1
#define CONFIG_SYS_CS0_FTIM2		CONFIG_SYS_NAND_FTIM2
#define CONFIG_SYS_CS0_FTIM3		CONFIG_SYS_NAND_FTIM3

#if 0
#define CONFIG_SYS_CSPR1_EXT		CONFIG_SYS_NOR_CSPR_EXT
#define CONFIG_SYS_CSPR1		CONFIG_SYS_NOR_CSPR
#define CONFIG_SYS_AMASK1		CONFIG_SYS_NOR_AMASK
#define CONFIG_SYS_CSOR1		CONFIG_SYS_NOR_CSOR
#define CONFIG_SYS_CS1_FTIM0		CONFIG_SYS_NOR_FTIM0
#define CONFIG_SYS_CS1_FTIM1		CONFIG_SYS_NOR_FTIM1
#define CONFIG_SYS_CS1_FTIM2		CONFIG_SYS_NOR_FTIM2
#define CONFIG_SYS_CS1_FTIM3		CONFIG_SYS_NOR_FTIM3
#endif 

#if 0
#define CONFIG_SYS_CSPR0_EXT		CONFIG_SYS_NOR_CSPR_EXT
#define CONFIG_SYS_CSPR0		CONFIG_SYS_NOR_CSPR
#define CONFIG_SYS_AMASK0		CONFIG_SYS_NOR_AMASK
#define CONFIG_SYS_CSOR0		CONFIG_SYS_NOR_CSOR
#define CONFIG_SYS_CS0_FTIM0		CONFIG_SYS_NOR_FTIM0
#define CONFIG_SYS_CS0_FTIM1		CONFIG_SYS_NOR_FTIM1
#define CONFIG_SYS_CS0_FTIM2		CONFIG_SYS_NOR_FTIM2
#define CONFIG_SYS_CS0_FTIM3		CONFIG_SYS_NOR_FTIM3

#define CONFIG_SYS_CSPR1_EXT		CONFIG_SYS_NAND_CSPR_EXT
#define CONFIG_SYS_CSPR1		CONFIG_SYS_NAND_CSPR
#define CONFIG_SYS_AMASK1		CONFIG_SYS_NAND_AMASK
#define CONFIG_SYS_CSOR1		CONFIG_SYS_NAND_CSOR
#define CONFIG_SYS_CS1_FTIM0		CONFIG_SYS_NAND_FTIM0
#define CONFIG_SYS_CS1_FTIM1		CONFIG_SYS_NAND_FTIM1
#define CONFIG_SYS_CS1_FTIM2		CONFIG_SYS_NAND_FTIM2
#define CONFIG_SYS_CS1_FTIM3		CONFIG_SYS_NAND_FTIM3
#endif

#if 0
#define CONFIG_SYS_CSPR2_EXT		CONFIG_SYS_CPLD_CSPR_EXT
#define CONFIG_SYS_CSPR2		CONFIG_SYS_CPLD_CSPR
#define CONFIG_SYS_AMASK2		CONFIG_SYS_CPLD_AMASK
#define CONFIG_SYS_CSOR2		CONFIG_SYS_CPLD_CSOR
#define CONFIG_SYS_CS2_FTIM0		CONFIG_SYS_CPLD_FTIM0
#define CONFIG_SYS_CS2_FTIM1		CONFIG_SYS_CPLD_FTIM1
#define CONFIG_SYS_CS2_FTIM2		CONFIG_SYS_CPLD_FTIM2
#define CONFIG_SYS_CS2_FTIM3		CONFIG_SYS_CPLD_FTIM3
#endif

/* EEPROM */
#ifndef SPL_NO_EEPROM
#endif

/*
 * Environment
 */
#ifndef SPL_NO_ENV
#define CONFIG_ENV_OVERWRITE
#endif

#if defined(CONFIG_ENV_IS_IN_NAND)
#define CONFIG_ENV_SIZE			0x2000
#define CONFIG_ENV_OFFSET		(10 * CONFIG_SYS_NAND_BLOCK_SIZE)
#define CONFIG_SYS_MMC_ENV_DEV		0
#elif defined(CONFIG_ENV_IS_IN_MMC)
#define CONFIG_ENV_OFFSET		(1024 * 1024)
#define CONFIG_SYS_MMC_ENV_DEV		0
#define CONFIG_ENV_SIZE			0x2000
#endif

/* FMan */
#ifndef SPL_NO_FMAN
#ifdef CONFIG_SYS_DPAA_FMAN
#define CONFIG_FMAN_ENET
#define CONFIG_PHYLIB
#define CONFIG_PHYLIB_10G
#define CONFIG_PHY_GIGE		/* Include GbE speed/duplex detection */

#define CONFIG_PHY_VITESSE
#define CONFIG_PHY_ATHEROS

#define RGMII_PHY1_ADDR			0x3

#define QSGMII_PORT1_PHY_ADDR		0x4
#define QSGMII_PORT2_PHY_ADDR		0x5
#define QSGMII_PORT3_PHY_ADDR		0x6
#define QSGMII_PORT4_PHY_ADDR		0x7

#define CONFIG_ETHPRIME			"FM1@DTSEC3"
#endif
#endif

/* QE */
#ifndef SPL_NO_QE
#if !defined(CONFIG_SD_BOOT) && !defined(CONFIG_NAND_BOOT) && \
	!defined(CONFIG_QSPI_BOOT)
#define CONFIG_U_QE
#endif
#endif

/* SATA */
#ifndef SPL_NO_SATA
#ifndef CONFIG_CMD_EXT2
#define CONFIG_CMD_EXT2
#endif
#define CONFIG_SYS_SCSI_MAX_SCSI_ID		2
#define CONFIG_SYS_SCSI_MAX_LUN		2
#define CONFIG_SYS_SCSI_MAX_DEVICE		(CONFIG_SYS_SCSI_MAX_SCSI_ID * \
				CONFIG_SYS_SCSI_MAX_LUN)
#define SCSI_VEND_ID 0x1b4b
#define SCSI_DEV_ID  0x9170
#define CONFIG_SCSI_DEV_LIST {SCSI_VEND_ID, SCSI_DEV_ID}
#endif

#define CONFIG_FAT_WRITE
#include <asm/fsl_secure_boot.h>

#undef CONFIG_CMD_IMLS
#undef CONFIG_CMD_SCSI

#endif /* __LS1043V_H__ */
